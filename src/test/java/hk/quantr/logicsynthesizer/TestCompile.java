/*
 * Copyright 2023 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logicsynthesizer;

import hk.quantr.javalib.CommonLib;
import hk.quantr.logicsynthesizer.antlr.VerilogLexer;
import hk.quantr.logicsynthesizer.antlr.VerilogParser;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import hk.quantr.logicsynthesizer.data.Data;
import java.io.File;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestCompile {

	@Test
	public void test() throws IOException, Exception {
		try {
			String str = IOUtils.toString(TestCompile.class.getClassLoader().getResourceAsStream("verilogexample/example1/WireExample.v"), "utf-8");
			Data data = new Data();

			// input output
			VerilogLexer lexer = new VerilogLexer(CharStreams.fromString(str));
			CommonTokenStream tokenStream = new CommonTokenStream(lexer);
			InputOutputListener inputOutputListener = new InputOutputListener(data);
			VerilogParser parser = new VerilogParser(tokenStream);
			parser.addParseListener(inputOutputListener);
			parser.source_text();
			// end input output

			// main
			System.out.println("-".repeat(100));
			lexer = new VerilogLexer(CharStreams.fromString(str));
			tokenStream = new CommonTokenStream(lexer);
			MainListener myListener = new MainListener(data);
			parser = new VerilogParser(tokenStream);
			parser.addParseListener(myListener);
			parser.source_text();
			System.out.println("-".repeat(100));
			data.dump();
			System.out.println("-".repeat(100));
			// end main

			System.out.println("-".repeat(100));
			GraphvizLib.export(data, new File(System.getProperty("user.home") + "/Desktop/a.dot"));
			CommonLib.runCommand("/usr/bin/dot -Tpng " + System.getProperty("user.home") + "/Desktop/a.dot" + " -o " + System.getProperty("user.home") + "/Desktop/a.png");
			System.out.println("-".repeat(100));
			Logisim.export(data, new File(System.getProperty("user.home") + "/Desktop/a.circ"));
		} catch (URISyntaxException ex) {
			Logger.getLogger(TestCompile.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
