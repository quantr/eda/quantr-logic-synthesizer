package hk.quantr.logicsynthesizer;

import hk.quantr.logicsynthesizer.antlr.VerilogLexer;
import hk.quantr.logicsynthesizer.antlr.VerilogParser;
import hk.quantr.logicsynthesizer.antlr.VerilogParserBaseListener;
import hk.quantr.logicsynthesizer.data.Data;
import java.io.IOException;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

public class TestListener {

	class MyListener extends VerilogParserBaseListener {


		@Override
		public void exitContinuous_assign(VerilogParser.Continuous_assignContext ctx) {
			System.out.println(ctx.list_of_net_assignments().net_assignment().get(0).expression().expression(0).getText());
		}

	}

	@Test
	public void test() throws IOException {
		String str = IOUtils.toString(TestListener.class.getClassLoader().getResourceAsStream("verilogexample/example1.1/WireExample.v"), "utf-8");

		VerilogLexer lexer = new VerilogLexer(CharStreams.fromString(str));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		
		
		
		MyListener listener = new MyListener();
		VerilogParser parser = new VerilogParser(tokenStream);
		parser.addParseListener(listener);
		parser.source_text();
	}
}
