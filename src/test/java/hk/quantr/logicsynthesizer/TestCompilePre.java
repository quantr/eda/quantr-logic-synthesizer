/*
 * Copyright 2023 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logicsynthesizer;

import hk.quantr.logicsynthesizer.antlr.VerilogLexer;
import hk.quantr.logicsynthesizer.antlr.VerilogPreParser;
import hk.quantr.logicsynthesizer.antlr.VerilogPreParserBaseListener;
import java.io.IOException;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestCompilePre {

	class MyListener extends VerilogPreParserBaseListener {

		@Override
		public void exitIfdef_directive(VerilogPreParser.Ifdef_directiveContext ctx) {
			System.out.println(">> " + ctx.getText());
		}

	}

	@Test
	public void test() throws IOException {
		String str = IOUtils.toString(TestCompile.class.getClassLoader().getResourceAsStream("verilogexample/tb.v"), "utf-8");
		VerilogLexer lexer = new VerilogLexer(CharStreams.fromString(str));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer, VerilogLexer.DIRECTIVES);
		tokenStream.fill();
		MyListener listener = new MyListener();
		VerilogPreParser parser = new VerilogPreParser(tokenStream);
		parser.addParseListener(listener);
		parser.source_text();
	}
}
