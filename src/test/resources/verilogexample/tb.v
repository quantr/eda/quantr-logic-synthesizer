module tb;
  reg clk, d, rstn;
  wire q;
  reg [3:0] delay;

  my_design u0 ( .clk(clk), .d(d),
`ifdef INCLUDE_RSTN asdasd asda s da
                .rstn(rstn),
`endif
                .q(q));

  always #10 clk = ~clk;

  initial begin

    {d, rstn, clk} = 0;

	#20 rstn = 1;
	
    for (integer i = 0 ; i < 20; i=i+1) begin
      delay = $random;
      #(delay) d = $random;
    end

    #20 $finish;
  end

endmodule