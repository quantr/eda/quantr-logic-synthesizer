module WireExample (BpW, Error, Wait, Valid, Clear);
	input Error, Wait;
	input Valid, Clear;
	output BpW;
	wire BpW;
	assign BpW = Error & Wait & (~Clear | Valid);
	//assign BpW = Valid | Clear;
endmodule
