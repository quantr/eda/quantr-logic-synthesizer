/*
 * Copyright 2023 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logicsynthesizer;

import hk.quantr.logicsynthesizer.antlr.VerilogParser;
import hk.quantr.logicsynthesizer.antlr.VerilogParserBaseListener;
import hk.quantr.logicsynthesizer.data.And2;
import hk.quantr.logicsynthesizer.data.Component;
import hk.quantr.logicsynthesizer.data.Data;
import hk.quantr.logicsynthesizer.data.Gate;
import hk.quantr.logicsynthesizer.data.Not;
import hk.quantr.logicsynthesizer.data.Or2;
import hk.quantr.logicsynthesizer.data.Output;
import hk.quantr.logicsynthesizer.data.Wire;
import java.util.Random;
import java.util.Stack;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class MainListener extends VerilogParserBaseListener {

	Stack<Component> expressionStack = new Stack();

	Data data;
	boolean hasUnaryOperator;

	public MainListener(Data data) {
		this.data = data;
	}

	@Override
	public void exitExpression(VerilogParser.ExpressionContext ctx) {
		String name = ctx.getText();
		System.out.println("exit expression = " + name + ", " + expressionStack.size());
		if (name.equals("~Clear|Valid")) {
			System.out.println("");
		}
		if (ctx.expression().size() == 0) {
			Component c = data.components.get(name);
			if (c != null) {
				expressionStack.push(c);
			} else {
				if (hasUnaryOperator) {
					hasUnaryOperator = false;
					Not notGate = new Not(name);
					notGate.input1 = data.components.get(ctx.primary().getText());
					data.components.put(name, notGate);
					expressionStack.push(notGate);
				}
			}
		} else if (ctx.expression().size() == 2) {
			System.out.println("\thandle: " + name);
			if (name.equals("Error&Wait&(~Clear|Valid)")) {
				System.out.println("");
			}
			Component c1 = expressionStack.pop();
			Component c2 = expressionStack.pop();
			if (ctx.VL() != null) {
				Or2 or2 = new Or2(name);
				if (c1 instanceof Gate) {
					Wire wire = new Wire("Wire " + new Random().nextInt(), ((Gate) c1).output, or2.input1);
					((Gate) c1).output = wire;
					or2.input1 = wire;
				} else {
					or2.input1 = c1;
				}

				if (c2 instanceof Gate) {
					Wire wire = new Wire("Wire " + new Random().nextInt(), ((Gate) c2).output, or2.input2);
					((Gate) c2).output = wire;
					or2.input2 = wire;
				} else {
					or2.input2 = c2;
				}
				data.components.put(name, or2);
				expressionStack.push(or2);
			} else if (ctx.AM() != null) {
				And2 and2 = new And2(name);
				if (c1 instanceof Gate) {
					Wire wire = new Wire("Wire " + new Random().nextInt(), ((Gate) c1).output, and2.input1);
					((Gate) c1).output = wire;
					and2.input1 = wire;
				} else {
					and2.input1 = c1;
				}

				if (c2 instanceof Gate) {
					Wire wire = new Wire(null, ((Gate) c2).output, and2.input2);
					((Gate) c2).output = wire;
					and2.input2 = wire;
				} else {
					and2.input2 = c2;
				}
				data.components.put(name, and2);
				expressionStack.push(and2);
			} else {
				Logicsynthesizer.logger.severe(String.format(Error.messages.get(2), name));
				System.exit(1);
			}
		} else {
			Logicsynthesizer.logger.severe(String.format(Error.messages.get(3), ctx.expression().size()));
			System.exit(1);
		}
	}

	@Override
	public void exitNet_assignment(VerilogParser.Net_assignmentContext ctx) {
		Component left = data.components.get(ctx.net_lvalue().getText());
		Component right = data.components.get(ctx.expression().getText());
		if (left instanceof Output) {
			Output leftOutput = (Output) left;
			leftOutput.inputs.add(right);
			if (right instanceof Gate) {
				((Gate) right).output = left;
			} else {
				Logicsynthesizer.logger.severe(String.format(Error.messages.get(2), null));
				System.exit(1);
			}
		} else {
			Logicsynthesizer.logger.severe(String.format(Error.messages.get(2), null));
			System.exit(1);
		}
	}

	@Override
	public void exitUnary_operator(VerilogParser.Unary_operatorContext ctx) {
		hasUnaryOperator = true;
	}

}
