package hk.quantr.logicsynthesizer;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class Logicsynthesizer {

	public static final Logger logger = Logger.getLogger(Logicsynthesizer.class.getName());

	static {
		InputStream stream = Logicsynthesizer.class.getClassLoader().getResourceAsStream("logging.properties");
		try {
			LogManager.getLogManager().readConfiguration(stream);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public static void main(String[] args) {
		System.out.println("Hello World!");
	}
}
