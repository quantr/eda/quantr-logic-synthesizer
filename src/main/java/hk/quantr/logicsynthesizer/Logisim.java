/*
 * Copyright 2023 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logicsynthesizer;

import com.thoughtworks.xstream.XStream;
import hk.quantr.logicsynthesizer.data.And2;
import hk.quantr.logicsynthesizer.data.Data;
import hk.quantr.logicsynthesizer.data.Nand2;
import hk.quantr.logicsynthesizer.data.Nor2;
import hk.quantr.logicsynthesizer.data.Not;
import hk.quantr.logicsynthesizer.data.Or2;
import hk.quantr.logicsynthesizer.data.Output;
import hk.quantr.logicsynthesizer.data.Pin;
import hk.quantr.logicsynthesizer.data.Xnor2;
import hk.quantr.logicsynthesizer.data.Xor2;
import hk.quantr.logisim.library.data.A;
import hk.quantr.logisim.library.data.Circuit;
import hk.quantr.logisim.library.data.Component;
import hk.quantr.logisim.library.data.Library;
import hk.quantr.logisim.library.data.Location;
import hk.quantr.logisim.library.data.Main;
import hk.quantr.logisim.library.data.Project;
import hk.quantr.logisim.library.data.Tool;
import hk.quantr.logisim.library.data.Wire;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.commons.io.FileUtils;
import hk.quantr.routingalgo.lee.Point;
import hk.quantr.routingalgo.lee.VerifyGrid;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class Logisim {

	public static void export(Data data, File file) throws IOException {
		int gridSize = 10;
		XStream xstream = new XStream();
		xstream.processAnnotations(Project.class);
		VerifyGrid circuitGrid = new VerifyGrid(gridSize);
		Project project = new Project("3.8.0", "1.0", new Circuit("main"));
		project.main = new Main("main");

		Circuit circuit = project.circuit;
//        MyAlgorithm myAlgorithm = new MyAlgorithm();
//		Component comp = new Component("Pin", "0", new Point(180, 170));
//		comp.a = new A("Component", "Component");
//		circuit.components.add(comp);
//		circuit.components.add(new Component("NOT Gate", "1", new Point(310, 170)));
//		circuit.components.add(new Component("LED", "5", new Point(440, 110)));

		int x = 0;
		int[] y = new int[20];

		for (hk.quantr.logicsynthesizer.data.Component c : data.components.values()) {
			if (c instanceof Pin) {
//                circuitGrid.setnodeLoc(180 / gridSize + x, y[0]);
				Component comp = new Component("Pin", "0", new Location(180 + x * 40, 100 + y[0] * 40));
				comp.a = new A(c.name, c.name);
				y[0]++;
				circuit.components.add(comp);
			} else if (c instanceof And2) {
//                circuitGrid.setnodeLoc(280/5 + x*40/5, y[1]);
				circuit.components.add(new Component("AND Gate", "1", new Location(280 + x * 40, 100 + y[1] * 40)));
				y[1] += 2;
			} else if (c instanceof Or2) {
//                circuitGrid.setnodeLoc(280/5 + x*40/5, y[2]);
				circuit.components.add(new Component("OR Gate", "1", new Location(280 + x * 40, 300 + y[2] * 40)));
				y[2] += 2;
			} else if (c instanceof Nand2) {
				circuit.components.add(new Component("NAND Gate", "1", new Location(280 + x * 40, 100 + y[3] * 40)));
//                circuit
				y[3] += 2;
			} else if (c instanceof Nor2) {
				circuit.components.add(new Component("NOR Gate", "1", new Location(280 + x * 40, 100 + y[4] * 40)));
				y[4] += 2;
			} else if (c instanceof Xor2) {
				circuit.components.add(new Component("XOR Gate", "1", new Location(280 + x * 40, 100 + y[5] * 40)));
				y[5] += 2;
			} else if (c instanceof Xnor2) {
				circuit.components.add(new Component("XNOR Gate", "1", new Location(280 + x * 40, 100 + y[6] * 40)));
				y[6] += 2;
			} else if (c instanceof Not) {
				circuit.components.add(new Component("NOT Gate", "1", new Location(280 + x * 40, 200 + y[7] * 40)));
				y[7]++;
			} else if (c instanceof Output) {
//                circuitGrid.setnodeLoc(x, y[8]);
				circuit.components.add(new Component("LED", "5", new Location(320 + x * 40, 100 + y[8] * 40)));
				y[8]++;
			}

		}
		circuit.components.add(new Component("AND Gate", "1", new Location(500, 50)));
		circuit.a.add(new A("appearance", "logisim_evolution"));
		circuit.a.add(new A("circuit", "main"));
		circuit.a.add(new A("circuitnamedboxfixedsize", "true"));
		circuit.a.add(new A("simulationFrequency", "2048.0"));
//        for (int i = 100; i < 600; i += gridSize) {
//            for (int j = 150; j < 250; j += gridSize) {
//                circuitGrid.wireCol[i / gridSize][ j / gridSize] = true;
//            }
//        }

		for (int i = 300; i > 0; i -= gridSize) {
			circuitGrid.wireCol[320 / gridSize][i / gridSize] = true;
			circuitGrid.wireCol[320 / gridSize + 1][i / gridSize] = true;
		}
		circuitGrid.wireRow[320 / gridSize][300 / gridSize] = true;
		circuitGrid.setnodeLoc(320 / gridSize + 1, 300 / gridSize);
		circuitGrid.setnodeLoc(320 / gridSize, 300 / gridSize);
//        circuitGrid.wireCol[320 / gridSize + 1][300 / gridSize] = true;
//        circuitGrid.wireCol[320 / gridSize - 1][300 / gridSize] = true;
//        circuitGrid.setnodeLoc(320 / gridSize - 1, 300 / gridSize);
//        circuitGrid.setnodeLoc(320 / gridSize + 1, 300 / gridSize);
//		Point start = new Point(300, 300);
//		Point end = new Point(350, 300);
//		draw(start, end, circuit, MyAlgorithm.myAlgorithm(start, end, 200));

		Library lib0 = new Library("0", "#Wiring");
		lib0.tool = new Tool("Pin");
		lib0.tool.a = new A("appearance", "classic");
		project.libraries.add(lib0);
		project.libraries.add(new Library("1", "#Gates"));
		project.libraries.add(new Library("5", "#I/O"));
		project.libraries.add(new Library("8", "#Base"));
		String xml = xstream.toXML(project);
		System.out.println(xml);

		FileUtils.writeStringToFile(file, xml, "utf-8");
	}

	public static void draw(Point start, Point end, Circuit circuit, ArrayList<Point> corners) {
//        if (corners == null) {
//            System.out.println("not found");
//            return;
//        }
		if (!corners.isEmpty()) {
			System.out.println(corners.get(0).x);
			circuit.wires.add(new Wire(new Location(end.x, end.y), new Location(corners.get(0).x, corners.get(0).y)));
			for (int i = 0; i < corners.size(); i++) {
				if (i == corners.size() - 1) {
					circuit.wires.add(new Wire(new Location(corners.get(i).x, corners.get(i).y), new Location(start.x, start.y)));
				} else {
					circuit.wires.add(new Wire(new Location(corners.get(i).x, corners.get(i).y), new Location(corners.get(i + 1).x, corners.get(i + 1).y)));
				}
			}
		} else {
			circuit.wires.add(new Wire(new Location(end.x, end.y), new Location(start.x, start.y)));
		}
	}
}
