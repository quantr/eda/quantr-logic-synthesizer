/*
 * Copyright 2023 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logicsynthesizer;

import hk.quantr.logicsynthesizer.antlr.VerilogParser;
import hk.quantr.logicsynthesizer.antlr.VerilogParserBaseListener;
import hk.quantr.logicsynthesizer.data.Data;
import hk.quantr.logicsynthesizer.data.Output;
import hk.quantr.logicsynthesizer.data.Pin;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class InputOutputListener extends VerilogParserBaseListener {

	Data data;

	public InputOutputListener(Data data) {
		this.data = data;
	}

	@Override
	public void exitInput_declaration(VerilogParser.Input_declarationContext ctx) {
		for (VerilogParser.Port_identifierContext c : ctx.list_of_port_identifiers().port_identifier()) {
			System.out.println("input : " + c.identifier().getText());
			String name = c.identifier().getText();
			Pin pin = new Pin(name);
			data.components.put(name, pin);
		}
	}

	@Override
	public void exitOutput_declaration(VerilogParser.Output_declarationContext ctx) {
		for (VerilogParser.Port_identifierContext c : ctx.list_of_port_identifiers().port_identifier()) {
			System.out.println("output : " + c.identifier().getText());
			String name = c.identifier().getText();
			Output pin = new Output(name);
			data.components.put(name, pin);
		}
	}
}
