/*
 * Copyright 2023 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logicsynthesizer;

import java.util.HashMap;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class Error {

	public static HashMap<Integer, String> messages = new HashMap();

	static {
		messages.put(1, "Component \"%s\" not exist");
		messages.put(2, "Logic component \"%s\" have not handle");
		messages.put(3, "Expression has %d of child expression, not handle");
	}

}
