/*
 * Copyright 2023 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logicsynthesizer.data;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public abstract class Component {

    static Output Output;

//	public enum TYPE {
//		GATE, PIN, OUTPUT
//	};
//
//	public enum GATE_TYPE {
//		AND, OR
//	};
	public String name;
//	public TYPE type;
//	public GATE_TYPE gateType;
//	public ArrayList<Component> inputs = new ArrayList();
//	public ArrayList<Component> outputs = new ArrayList();

//	@Override
//	public String toString() {
//		return "Component{" + "name=" + name + ", type=" + type + ", gateType=" + gateType + ", inputs=" + inputs + ", outputs=" + outputs + '}';
//	}
	public Component(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Component{" + "name=" + name + '}';
	}

}
