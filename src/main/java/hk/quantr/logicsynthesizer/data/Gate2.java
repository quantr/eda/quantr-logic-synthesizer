/*
 * Copyright 2023 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logicsynthesizer.data;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public abstract class Gate2 extends Gate {

	public Component input1;
	public Component input2;

	public Gate2(String name) {
		super(name);
	}

	@Override
	public String toString() {
		return "Gate2{" + "input1=" + input1 + ", input2=" + input2 + '}';
	}

}
