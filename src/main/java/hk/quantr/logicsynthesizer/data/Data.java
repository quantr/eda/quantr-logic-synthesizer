/*
 * Copyright 2023 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logicsynthesizer.data;

import java.util.HashMap;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class Data {

	public HashMap<String, Component> components = new HashMap();

//	public void createOr2(String input1, String input2, String output) {
//		String name = "or_" + components.size();
//		Or2 c = new Or2(name);
////		c.type = TYPE.GATE;
////		c.gateType = GATE_TYPE.OR;
////		if (components.get(input1) != null) {
////			c.inputs.add(components.get(input1));
////		}
////		if (components.get(input2) != null) {
////			c.inputs.add(components.get(input2));
////		}
////		if (components.get(output) != null) {
////			c.outputs.add(components.get(output));
////		}
//		components.put(name, c);
//	}
//
//	public void createAnd2(String input1, String input2, String output) {
//		String name = "and_" + components.size();
//		And2 c = new And2(name);
////		c.type = TYPE.GATE;
////		c.gateType = GATE_TYPE.AND;
////		if (components.get(input1) != null) {
////			c.inputs.add(components.get(input1));
////		}
////		if (components.get(input2) != null) {
////			c.inputs.add(components.get(input2));
////		}
////		if (components.get(output) != null) {
////			c.outputs.add(components.get(output));
////		}
//		components.put(name, c);
//	}
//
//	public void createPin(String name) {
//		Pin c = new Pin(name);
//		components.put(name, c);
//	}
//
//	public void createOutput(String name) {
//		Output c = new Output(name);
//		components.put(name, c);
//	}

	public void dump() {
		for (Component c : components.values()) {
			System.out.println(c);
		}
	}

}
