/*
 * Copyright 2023 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logicsynthesizer;

import hk.quantr.logicsynthesizer.data.Component;
import hk.quantr.logicsynthesizer.data.Data;
import hk.quantr.logicsynthesizer.data.Gate1;
import hk.quantr.logicsynthesizer.data.Gate2;
import hk.quantr.logicsynthesizer.data.Output;
import hk.quantr.logicsynthesizer.data.Pin;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import org.apache.commons.text.StringSubstitutor;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class GraphvizLib {

	public static void export(Data data, File file) throws URISyntaxException, IOException {
		String template = Files.readString(Paths.get(GraphvizLib.class.getResource("dot.template").toURI()), Charset.forName("utf-8"));

		// nodeDefine
		String nodeDefine = "";
		String inputs = "";
		String outputs = "";
		String gates = "";

		for (Component c : data.components.values()) {
			if (c instanceof Pin) {
				nodeDefine += "\"" + c.name + "\"" + " [\n"
						+ "\n"
						+ "]\n";
				inputs += "\"" + c.name + "\"" + ";";
			} else if (c instanceof Gate1) {
				nodeDefine += "\"" + c.name + "\"" + " [\n"
						+ "     fillcolor = white,\n"
						+ "     fixedsize = true,\n"
						+ "     label = \"" + c.getClass().getSimpleName() + "\",\n"
						+ "]\n";
				gates += "\"" + c.name + "\"" + ";";
			} else if (c instanceof Gate2) {
				nodeDefine += "\"" + c.name + "\"" + " [\n"
						+ "     fillcolor = white,\n"
						+ "     fixedsize = true,\n"
						+ "     label = \"" + c.getClass().getSimpleName() + "\",\n"
						+ "]\n";
				gates += "\"" + c.name + "\"" + ";";
			} else if (c instanceof Output) {
				nodeDefine += "\"" + c.name + "\"" + " [\n"
						+ "     color = \"#00d5ff\",\n"
						+ "     fixedsize = true,\n"
						+ "]\n";
				outputs += "\"" + c.name + "\"" + ";";
			} else {
				nodeDefine += "\"" + c.name + "\"" + " [\n"
						+ "     fillcolor = red,\n"
						+ "     fixedsize = true,\n"
						+ "]\n";
			}
		}
		// end nodeDefine

		// nodeMapping
		String nodeMapping = "";
		for (Component c : data.components.values()) {
			if (c instanceof Gate2) {
				Gate2 g = (Gate2) c;
				if (g.name.equals("Wait|Clear|Error")) {
					System.out.println("");
				}
				try {
					nodeMapping += "\"" + g.input1.name + "\"" + " -> " + "\"" + c.name + "\"" + " [color = \"#ff8c00\"]\n";
					nodeMapping += "\"" + g.input2.name + "\"" + " -> " + "\"" + c.name + "\"" + " [color = \"#ff8c00\"]\n";
					nodeMapping += "\"" + c.name + "\"" + " -> " + "\"" + g.output.name + "\"" + " [color = \"#ff8c00\"]\n";
				} catch (Exception ex) {
					System.err.println("error: " + g.name);
					ex.printStackTrace();
					System.exit(123);
				}
			} else if (c instanceof Gate1) {
				Gate1 g = (Gate1) c;
				nodeMapping += "\"" + g.input1.name + "\"" + " -> " + "\"" + c.name + "\"" + " [color = \"#ff8c00\"]\n";
				nodeMapping += "\"" + c.name + "\"" + " -> " + "\"" + g.output.name + "\"" + " [color = \"#ff8c00\"]\n";
			} else if (c instanceof Pin) {
			} else if (c instanceof Output) {
			} else if (c instanceof Component) {
			} else {
				System.err.println("fuck, not support " + c);
				System.exit(1);
			}
		}
		// end nodeMapping

		Map<String, String> valuesMap = new HashMap();
		valuesMap.put("inputs", inputs);
		valuesMap.put("outputs", outputs);
		valuesMap.put("gates", gates);
		valuesMap.put("nodeDefine", nodeDefine);
		valuesMap.put("nodeMapping", nodeMapping);

		StringSubstitutor sub = new StringSubstitutor(valuesMap);
		String resolvedString = sub.replace(template);
		System.out.println(resolvedString);
		FileUtils.write(file, resolvedString, "utf-8");
	}
}
